# FoxRMM Windows PowerShell Installer 1.0
# Written by MC
# ch3Lyto@gmail.com
#
# 3 Feb 2022

Write-Host "Installing Zabbix Components..." ; Write-Host ""

# Set Higher TLS Level for Older PowerShell Instances
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# Remove any old Salt Master keys, for safety
If ((Test-Path c:\salt\conf\pki\minion\minion_master.pub) -eq $True) {Rename-Item c:\salt\conf\pki\minion\minion_master.pub minion_master.pub.old}

# Install Chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')) 

# Install Zabbix Agent via Chocolatey and configure with the Minion ID
net stop "Zabbix Agent"
choco uninstall zabbix-agent.install -y
choco install zabbix-agent.install -y
net stop "Zabbix Agent"

# Generate Zabbix Config
$HostName = Read-Host "Please enter Device Name and Mesh Central tag, example: Computer_tagmeschentral"
$zxconf = @"
LogType=file
LogFile=C:\Program Files\Zabbix Agent\zabbix_agentd.log
DenyKey=system.run[*]
Server=zx.foxrmm.com
ListenPort=10051
ServerActive=zx.foxrmm.com
Timeout=3
Hostname=$HostName
"@
Set-Content -Path 'c:\Program Files\Zabbix Agent\zabbix_agentd.conf' -Value $zxconf
net start "Zabbix Agent"

Write-Host "Installation is now complete."

# Inform the user of the Minion ID for this host
Write-Host ""
Write-Host $minion_id

# Pass Info on Screen
Write-Host ""
Write-Host "CCW Technology"
Write-Host "Zabbix installer was installed succesfully"
Write-Host ""
